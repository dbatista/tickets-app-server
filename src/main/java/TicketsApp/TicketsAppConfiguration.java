package TicketsApp;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class TicketsAppConfiguration extends Configuration {

    @JsonProperty
    private String serverName;

    @Valid
    @NotNull
    @JsonProperty
    private DataSourceFactory database = new DataSourceFactory();

    public String getServerName() {
        return this.serverName;
    }

    public DataSourceFactory getDataSourceFactory() {
        return database;
    }


}
