package TicketsApp;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class TicketsAppApplication extends Application<TicketsAppConfiguration> {

    public static void main(final String[] args) throws Exception {
        new TicketsAppApplication().run(args);
    }

    @Override
    public String getName() {
        return "TicketsApp";
    }

    @Override
    public void initialize(final Bootstrap<TicketsAppConfiguration> bootstrap) {
        // TODO: application initialization
    }

    @Override
    public void run(final TicketsAppConfiguration configuration,
                    final Environment environment) {
        // TODO: implement application
    }

}
